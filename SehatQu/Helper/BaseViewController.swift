//
//  BaseViewController.swift
//  SehatQu
//
//  Created by Macintosh on 27/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func showLoadingView() {
           if let window = UIApplication.shared.keyWindow {
               LoadingOverlay.shared.showOverlay(inView: window)
           } else {
               LoadingOverlay.shared.showOverlay(inView: self.navigationController!.view)
           }
       }
       
       func hideLoadingView() {
           LoadingOverlay.shared.hideOverlayView()
       }

    public func showAlertWith(title: String, message: String, action: (() -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "OK".uppercased(), style: UIAlertAction.Style.cancel, handler: { (alertAction) in
            if let action = action {
                action()
            }
        })
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func navigationBarTitle(withTitle: String) {
        let titleLabel = UILabel(frame: .zero)
        titleLabel.textColor = UIColor.black
        titleLabel.numberOfLines = 0
        titleLabel.text = ""
        
        
        titleLabel.text = withTitle
        titleLabel.font = UIFont.appFont(fontType: FontType.bold, fontSize: FontSize.L)
        titleLabel.sizeToFit()
        self.navigationItem.titleView = titleLabel
    }
}
