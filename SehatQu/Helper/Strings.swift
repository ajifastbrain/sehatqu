//
//  Strings.swift
//  SehatQu
//
//  Created by Aji Prakosa on 26/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation

struct Localization {
    struct Label {
        static let LOGIN = "Login".localized
        static let REMEMBER = "Remember Me".localized
        static let SOMETHING_WENT_WRONG = "Something Went Wrong".localized
        static let BUY = "Buy".localized
        static let PURCHASE_HISTORY = "Purchase History".localized
        static let SEARCH = "Search".localized
        static let NO_STORE_MATCH_YOUR_QUERY = "No product match your search query.".localized
    }
}
