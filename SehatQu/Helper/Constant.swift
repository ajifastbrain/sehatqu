//
//  Constant.swift
//  SehatQu
//
//  Created by Aji Prakosa on 26/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation

struct defaultsKeys {
    static let keyLogin = "keyLogin"
}

struct SehatQuNotification {
    static let tokenExpired     = "NOTIFICATION_TOKEN_EXPIRED"
    static let pinChanged       = "NOTIFICATION_PIN_CHANGED"
}

struct Size {
    static let headerHeight: CGFloat        = 65
    static let tabBarHeight: CGFloat        = 44
    static let statusBarHeight: CGFloat     = 20
    static let cardDivider: CGFloat         = 1.55
    static let actionButtonHeight: CGFloat  = 56
    static let tableCellHeight: CGFloat     = 56
    static let cardInfoViewHeight: CGFloat  = 30
    static let defaultPadding: CGFloat      = 16
}

struct APIEndPoint {
    static let home         = "/home"
}

