//
//  Connectivity.swift
//  SehatQu
//
//  Created by Aji Prakosa on 26/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
import Alamofire

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
