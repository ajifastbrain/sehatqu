//
//  DataModel.swift
//  SehatQu
//
//  Created by Aji Prakosa on 27/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import ObjectMapper

class DataModel: Mappable {
    var data: Data?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        data <- map["data"]
    }
}

class Data: Mappable {
    var category: [Category]?
    var productPromo: [ProductPromo]?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        category <- map["category"]
        productPromo <- map["productPromo"]
    }
}

class Category: Mappable {
    var imageUrl: String?
    var id: Int?
    var name: String?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        imageUrl <- map["imageUrl"]
        id <- map["id"]
        name <- map["name"]
    }
}


class ProductPromo: Mappable {
    var id: String?
    var imageUrl: String?
    var title: String?
    var description: String?
    var price: String?
    var loved: Int?
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        imageUrl <- map["imageUrl"]
        title <- map["title"]
        description <- map["description"]
        price <- map["price"]
        loved <- map["loved"]
    }
}
