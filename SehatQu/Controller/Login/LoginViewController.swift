//
//  LoginViewController.swift
//  SehatQu
//
//  Created by Macintosh on 26/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import FBSDKShareKit
import FBSDKLoginKit
import FBSDKCoreKit
import FirebaseCore
import Firebase
import GoogleSignIn

class LoginViewController: UIViewController, GIDSignInUIDelegate {
    

    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var txtUserName: JVFloatLabeledTextField!
    
    @IBOutlet weak var txtPassword: JVFloatLabeledTextField!
    
    @IBOutlet weak var viewFrame: UIView!
    
    @IBOutlet weak var labelRememberMe: UILabel!
    
    @IBOutlet weak var imgChecked: UIImageView!
    
    private var checkRemember: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelTitle.font = UIFont.appFont(fontType: FontType.bold, fontSize: FontSize.XXL)
        labelTitle.text = Localization.Label.LOGIN.uppercased()
        
        txtUserName.addBottomBorderWithColor(color: UIColor.init(hexString: "#848383"), width: 1)
        txtPassword.addBottomBorderWithColor(color: UIColor.init(hexString: "#848383"), width: 1)
        
        viewFrame.layer.borderWidth = 2
        viewFrame.layer.borderColor = UIColor.init(hexString: "#d4d3d3").cgColor
        viewFrame.layer.cornerRadius = 10
        
        labelRememberMe.font = UIFont.appFont(fontType: FontType.bold, fontSize: FontSize.SS)
        labelRememberMe.text = Localization.Label.REMEMBER
        
        NotificationCenter.default.addObserver(forName: .AccessTokenDidChange, object: nil, queue: OperationQueue.main) { (notification) in
            
            // Print out access token
            print("FB Access Token: \(String(describing: AccessToken.current?.tokenString))")
        }
        GIDSignIn.sharedInstance().uiDelegate = self
    }


    @IBAction func signInTapped(_ sender: UIButton) {
        let defaults = UserDefaults.standard
        defaults.set("1", forKey: defaultsKeys.keyLogin)
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
            delegate.instantiateViewController()
        }
    }
    
    
    @IBAction func checkRememberMe(_ sender: UIButton) {
        if checkRemember == false {
            imgChecked.image = UIImage(named: "checked")
            checkRemember = true
        } else {
            imgChecked.image = UIImage(named: "unchecked")
            checkRemember = false
        }
    }
    
    @IBAction func FBLoginTapped(_ sender: UIButton) {
        facebooklogin()
    }
    
    
    
    func facebooklogin() {
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["public_profile", "email", "user_friends"], from: self, handler: { (result, error) -> Void in
            print("\n\n result: \(String(describing: result))")
            print("\n\n Error: \(String(describing: error))")

            if (error == nil) {
                let fbloginresult : LoginManagerLoginResult = result!
                if(fbloginresult.isCancelled) {
                    print("Show Cancel alert")
                } else if(fbloginresult.grantedPermissions.contains("email")) {
                    self.returnUserData()
                }
            }
        })
        fbLoginManager.logOut()
    }
    
    func returnUserData() {
        let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields":"email,name"])
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            if ((error) != nil) {
                // Process error
                print("\n\n Error: \(String(describing: error))")
          } else {
                let defaults = UserDefaults.standard
                defaults.set("1", forKey: defaultsKeys.keyLogin)
                if let delegate = UIApplication.shared.delegate as? AppDelegate {
                    delegate.instantiateViewController()
                }

                let resultDic = result as! NSDictionary
                if (resultDic.value(forKey:"name") != nil) {
                    let userName = resultDic.value(forKey:"name")! as! String as NSString?
                    print("\n User Name is: \(String(describing: userName))")
                 }

                if (resultDic.value(forKey:"email") != nil) {
                    let userEmail = resultDic.value(forKey:"email")! as! String as NSString?
                    print("\n User Email is: \(String(describing: userEmail))")
                 }
            }
       })
    }
    
    @IBAction func googleSignInTapped(_ sender: UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    
    
}
