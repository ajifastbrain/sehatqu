//
//  HomeTabBarViewController.swift
//  SehatQu
//
//  Created by Aji Prakosa on 27/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import Foundation
import UIKit
@objc class HomeTabBarViewController: UITabBarController, UITabBarControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Home"
        self.navigationController?.navigationBar.isHidden = false
        self.showTitleTabBar()
        self.delegate = self
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print(item)
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if viewController.title! == "Home" {
            navigationBarTitle(withTitle: "Home")
            self.navigationItem.setHidesBackButton(true, animated:true);
        } else if viewController.title! == "Feed" {
            navigationBarTitle(withTitle: "Feed")
            self.navigationItem.setHidesBackButton(true, animated:true);
        } else if viewController.title! == "Cart" {
            navigationBarTitle(withTitle: "Cart")
            self.navigationItem.setHidesBackButton(true, animated:true);
        } else if viewController.title! == "Profile" {
            navigationBarTitle(withTitle: "Profile")
            self.navigationItem.setHidesBackButton(true, animated:true);
        }
    }
   
    func navigationBarTitle(withTitle: String) {
        let titleLabel = UILabel(frame: .zero)
        titleLabel.textColor = UIColor.black
        titleLabel.numberOfLines = 0
        titleLabel.text = ""
        
        
        titleLabel.text = withTitle
        titleLabel.font = UIFont.appFont(fontType: FontType.bold, fontSize: FontSize.L)
        titleLabel.sizeToFit()
        self.navigationItem.titleView = titleLabel
    }
    
    
    func showTitleTabBar() {
        // Create Tab one
        let tabOne = HomeViewController()
        let tabOneBarItem = UITabBarItem(title: "Home", image: UIImage(named: "homeico"), selectedImage: nil)
        tabOne.tabBarItem = tabOneBarItem
        
        
        // Create Tab two
        let tabTwo = FeedViewController()
        let tabTwoBarItem2 = UITabBarItem(title: "Feed", image: UIImage(named: "feedIcon"), selectedImage: nil)
        tabTwo.tabBarItem = tabTwoBarItem2
        
        
        // Create Tab Three
        let tabThree = CartViewController()
        let tabTwoBarItem3 = UITabBarItem(title: "Cart", image: UIImage(named: "cartIco"), selectedImage: nil)
        tabThree.tabBarItem = tabTwoBarItem3
        
        
        // Create Tab Four
        let tabFour = ProfileViewController()
        let tabTwoBarItem4 = UITabBarItem(title: "History", image: UIImage(named: "profileIco"), selectedImage: nil)
        tabFour.tabBarItem = tabTwoBarItem4
        self.viewControllers = [tabOne, tabTwo, tabThree, tabFour]
    }

    
}
