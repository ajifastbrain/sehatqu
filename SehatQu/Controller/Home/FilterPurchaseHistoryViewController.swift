//
//  FilterPurchaseHistoryViewController.swift
//  SehatQu
//
//  Created by Macintosh on 27/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class FilterPurchaseHistoryViewController: BaseViewController, UISearchBarDelegate {

    @IBOutlet weak var mainTable: UITableView!
    
    private var listProductPromo: [ProductPromo]?
    
    private var filteredProductPromo: [ProductPromo]?
    
    private var errorString: String?
    
    let searchController = UISearchController(searchResultsController: nil)
    private var searchBar = UISearchBar()
    
    func searchBarSetup() {
        searchBar.frame = CGRect(x:0,y:0,width:(UIScreen.main.bounds.width),height:Size.headerHeight)
        searchBar.showsScopeBar = true
        searchBar.selectedScopeButtonIndex = 0
        searchBar.delegate = self
        searchBar.placeholder = Localization.Label.SEARCH
        mainTable.tableHeaderView = searchBar
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Localization.Label.PURCHASE_HISTORY
        mainTable.register(UINib(nibName: "PurchaseCell", bundle: nil), forCellReuseIdentifier: "PurchaseCell")
        
        mainTable.delegate = self
        mainTable.dataSource = self
        mainTable.separatorStyle = .none
        mainTable.reloadData()
        searchBarSetup()
    }


    convenience init(listProductPromo: [ProductPromo]?) {
        self.init()
        self.listProductPromo = listProductPromo
        self.filteredProductPromo = listProductPromo
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.filterPromos(text: searchText)
        self.mainTable.reloadData()
    }
    
    func filterPromos(text: String) {
        if text.length() > 0 {
            self.errorString = nil
            
            self.filteredProductPromo = self.listProductPromo?.filter({ (elm) -> Bool in
                (elm.title?.lowercased().contains(text.lowercased()))! || (elm.description?.lowercased().contains(text.lowercased()))!
            })
            if self.filteredProductPromo?.count == 0 {
                self.errorString = Localization.Label.NO_STORE_MATCH_YOUR_QUERY
            }
        } else {
            self.filteredProductPromo = self.listProductPromo
        }
        
        _ = self.filteredProductPromo?.sorted {
            var isSorted = false
            if let first = $0.title, let second = $1.title {
                isSorted = first < second
            }
            return isSorted
        }
    }

}


extension FilterPurchaseHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let promo = self.filteredProductPromo {
            return promo.count
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cells: UITableViewCell?
        cells = mainTable.dequeueReusableCell(withIdentifier: "PurchaseCell", for: indexPath as IndexPath) as! PurchaseCell
        let cell = cells as! PurchaseCell
        let titles: String = self.filteredProductPromo?[indexPath.row].title ?? ""
        let URLImg: String = self.filteredProductPromo?[indexPath.row].imageUrl ?? ""
        let price: String = self.filteredProductPromo?[indexPath.row].price ?? ""
        cell.configureView(imgURL: URLImg, name: titles, price: price)
        cell.selectionStyle = .none
        return cells!
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let titles: String = self.filteredProductPromo?[indexPath.row].title ?? ""
        let URLImg: String = self.filteredProductPromo?[indexPath.row].imageUrl ?? ""
        let loved: Int = self.filteredProductPromo?[indexPath.row].loved ?? 0
        let desc: String = self.filteredProductPromo?[indexPath.row].description ?? ""
        let price: String = self.filteredProductPromo?[indexPath.row].price ?? ""
        self.navigationController?.pushViewController(DetailProductPromoViewController(imgURL: URLImg, title: titles, loved: loved, description: desc, price: price, listProductPromo: self.listProductPromo), animated: true)
    }
}
