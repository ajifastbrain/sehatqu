//
//  DetailProductPromoViewController.swift
//  SehatQu
//
//  Created by Aji Prakosa on 27/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class DetailProductPromoViewController: BaseViewController {
    
    @IBOutlet weak var mainTable: UITableView!
    
    private var imgURL = String()
    
    private var nameProduct = String()
    
    private var loved = Int()
    
    private var desc = String()
    
    private var price = String()
    
    private var listProductPromo: [ProductPromo]?
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        
        mainTable.register(UINib(nibName: "HeaderImageCell", bundle: nil), forCellReuseIdentifier: "HeaderImageCell")
        mainTable.register(UINib(nibName: "NameProductCell", bundle: nil), forCellReuseIdentifier: "NameProductCell")
        mainTable.register(UINib(nibName: "DescriptionViewCell", bundle: nil), forCellReuseIdentifier: "DescriptionViewCell")
        
        mainTable.register(UINib(nibName: "BuyPriceCell", bundle: nil), forCellReuseIdentifier: "BuyPriceCell")
        
        mainTable.delegate = self
        mainTable.dataSource = self
        mainTable.separatorStyle = .none
        mainTable.reloadData()
        
    }

    convenience init(imgURL: String, title: String, loved: Int, description: String, price: String, listProductPromo: [ProductPromo]?) {
        self.init()
        self.imgURL = imgURL
        self.nameProduct = title
        self.loved = loved
        self.desc = description
        self.price = price
        self.listProductPromo = listProductPromo
    }
    
    @objc func onBack(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func onShare(sender: UIButton) {
        let activityViewController : UIActivityViewController = UIActivityViewController(
            activityItems: [self.imgURL, self.nameProduct, self.desc, self.price], applicationActivities: nil)

        activityViewController.popoverPresentationController?.sourceView = (sender )

        activityViewController.popoverPresentationController?.permittedArrowDirections = .up
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: 150, y: 150, width: 0, height: 0)

        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.postToWeibo,
            UIActivity.ActivityType.print,
            UIActivity.ActivityType.assignToContact,
            UIActivity.ActivityType.saveToCameraRoll,
            UIActivity.ActivityType.addToReadingList,
            UIActivity.ActivityType.postToFlickr,
            UIActivity.ActivityType.postToVimeo,
            UIActivity.ActivityType.postToTencentWeibo
        ]
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @objc func onBuy(sender: UIButton) {
        let listPurchase = PurchaceHistoryViewController(listProductPromo: self.listProductPromo)
        self.navigationController?.pushViewController(listPurchase, animated: true)
    }
}


extension DetailProductPromoViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numRows:Int = Int()
        switch section {
        case 0:
            numRows = 1
        case 1:
            numRows = 1
        case 2:
            numRows = 1
        case 3:
            numRows = 1
        default:
            break
        }
        return numRows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                cell = mainTable.dequeueReusableCell(withIdentifier: "HeaderImageCell", for: indexPath as IndexPath) as UITableViewCell
                let cell = cell as! HeaderImageCell
                cell.configureImage(imgURL: self.imgURL)
                cell.btnBack.addTarget(self, action: #selector(onBack(sender:)), for: .touchUpInside)
                cell.btnShare.addTarget(self, action: #selector(onShare(sender:)), for: .touchUpInside)
                cell.selectionStyle = .none
            default:
                break
            }
        case 1:
            switch indexPath.row {
            case 0:
                cell = mainTable.dequeueReusableCell(withIdentifier: "NameProductCell", for: indexPath as IndexPath) as UITableViewCell
                let cell = cell as! NameProductCell
                cell.configureView(name: self.nameProduct, loved: self.loved)
                cell.selectionStyle = .none
            default:
                break
            }
        case 2:
            switch indexPath.row {
            case 0:
                cell = mainTable.dequeueReusableCell(withIdentifier: "DescriptionViewCell", for: indexPath as IndexPath) as UITableViewCell
                let cell = cell as! DescriptionViewCell
                cell.labelDesc.text = self.desc
                cell.selectionStyle = .none
            default:
                break
            }
        case 3:
            switch indexPath.row {
            case 0:
                cell = mainTable.dequeueReusableCell(withIdentifier: "BuyPriceCell", for: indexPath as IndexPath) as UITableViewCell
                let cell = cell as! BuyPriceCell
                cell.labelPrice.text = self.price
                cell.btnBuy.addTarget(self, action: #selector(onBuy(sender:)), for: .touchUpInside)
                cell.selectionStyle = .none
            default:
                break
            }

        default:
            break
        }
        return cell!
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var heightForRows: CGFloat = CGFloat()
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                heightForRows = 223
            default:
                break
            }
        case 1:
            switch indexPath.row {
            case 0:
                heightForRows = UITableView.automaticDimension
            default:
                break
            }
        case 2:
            switch indexPath.row {
            case 0:
                heightForRows = UITableView.automaticDimension
            default:
                break
            }
        case 3:
            switch indexPath.row {
            case 0:
                heightForRows = 69
            default:
                break
            }

        default:
            break
        }
        return heightForRows
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 2:
            self.navigationController?.pushViewController(DetailProductPromoViewController(), animated: true)
        default:
            break
        }
    }

}
