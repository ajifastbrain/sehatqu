//
//  PurchaceHistoryViewController.swift
//  SehatQu
//
//  Created by Aji Prakosa on 27/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class PurchaceHistoryViewController: BaseViewController {

    @IBOutlet weak var mainTable: UITableView!
    
    private var listProductPromo: [ProductPromo]?
    
    private var numRowx = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Localization.Label.PURCHASE_HISTORY
        mainTable.register(UINib(nibName: "PurchaseCell", bundle: nil), forCellReuseIdentifier: "PurchaseCell")
        
        mainTable.delegate = self
        mainTable.dataSource = self
        mainTable.separatorStyle = .none
        mainTable.reloadData()
    }
    
    convenience init(listProductPromo: [ProductPromo]?) {
        self.init()
        self.listProductPromo = listProductPromo
        self.numRowx = self.listProductPromo?.count ?? 0
    }

}

extension PurchaceHistoryViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return numRowx
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cells: UITableViewCell?
        cells = mainTable.dequeueReusableCell(withIdentifier: "PurchaseCell", for: indexPath as IndexPath) as! PurchaseCell
        let cell = cells as! PurchaseCell
        let titles: String = self.listProductPromo?[indexPath.row].title ?? ""
        let URLImg: String = self.listProductPromo?[indexPath.row].imageUrl ?? ""
        let price: String = self.listProductPromo?[indexPath.row].price ?? ""
        cell.configureView(imgURL: URLImg, name: titles, price: price)
        cell.selectionStyle = .none
        return cells!
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let titles: String = self.listProductPromo?[indexPath.row].title ?? ""
        let URLImg: String = self.listProductPromo?[indexPath.row].imageUrl ?? ""
        let loved: Int = self.listProductPromo?[indexPath.row].loved ?? 0
        let desc: String = self.listProductPromo?[indexPath.row].description ?? ""
        let price: String = self.listProductPromo?[indexPath.row].price ?? ""
        self.navigationController?.pushViewController(DetailProductPromoViewController(imgURL: URLImg, title: titles, loved: loved, description: desc, price: price, listProductPromo: self.listProductPromo), animated: true)
    }
}
