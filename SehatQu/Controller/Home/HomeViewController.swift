//
//  HomeViewController.swift
//  SehatQu
//
//  Created by Aji Prakosa on 26/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import ObjectMapper


class HomeViewController: BaseViewController {
    private var homeListModels: [DataModel]?
    
    private var listCategory: [Category]?
    
    private var listProductPromo: [ProductPromo]?
    
    @IBOutlet weak var mainTable: UITableView!
    
    private var numRowx = Int()
    
    override func viewWillAppear(_ animated: Bool) {
        self.showLoadingView()
        API.getHomeList(completion: { (dataModelList, error) in
             if let dataModel = dataModelList {
                 self.homeListModels = dataModel
                 self.listCategory = self.homeListModels?[0].data?.category
                 self.listProductPromo = self.homeListModels?[0].data?.productPromo
                 self.numRowx = self.listProductPromo?.count ?? 0
                 self.mainTable.reloadData()
                 self.hideLoadingView()
             } else if let error = error {
                self.hideLoadingView()
                self.showAlertWith(title: "Error", message: error, action: nil)
             }
        })
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Home"
        navigationBarTitle(withTitle: "Home")
        self.navigationController?.navigationBar.isHidden = true
        
        mainTable.register(UINib(nibName: "SearchBarCell", bundle: nil), forCellReuseIdentifier: "SearchBarCell")
        mainTable.register(UINib(nibName: "CategoryCell", bundle: nil), forCellReuseIdentifier: "CategoryCell")
        mainTable.register(UINib(nibName: "ProductPromoCell", bundle: nil), forCellReuseIdentifier: "ProductPromoCell")
        
        mainTable.delegate = self
        mainTable.dataSource = self
        mainTable.separatorStyle = .none
    }
    
    @objc func onSearch(sender: UIButton) {
        self.navigationController?.pushViewController(FilterPurchaseHistoryViewController(listProductPromo: listProductPromo), animated: true)
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var numRows:Int = Int()
        switch section {
        case 0:
            numRows = 1
        case 1:
            numRows = 1
        case 2:
            numRows = numRowx
        default:
            break
        }
        return numRows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell?
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                cell = mainTable.dequeueReusableCell(withIdentifier: "SearchBarCell", for: indexPath as IndexPath) as UITableViewCell
                let cell = cell as! SearchBarCell
                cell.btnSearch.addTarget(self, action: #selector(onSearch(sender:)), for: .touchUpInside)
                cell.selectionStyle = .none
            default:
                break
            }
        case 1:
            switch indexPath.row {
            case 0:
                cell = mainTable.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath as IndexPath) as UITableViewCell
                let cell = cell as! CategoryCell
                cell.configureView(dataCtg: self.listCategory ?? nil)
                cell.selectionStyle = .none
            default:
                break
            }
        case 2:
            cell = mainTable.dequeueReusableCell(withIdentifier: "ProductPromoCell", for: indexPath as IndexPath) as! ProductPromoCell
            let cell = cell as! ProductPromoCell
            let titles: String = self.listProductPromo?[indexPath.row].title ?? ""
            let URLImg: String = self.listProductPromo?[indexPath.row].imageUrl ?? ""
            let loved: Int = self.listProductPromo?[indexPath.row].loved ?? 0
            cell.configureView(imgURL: URLImg, name: titles, loved: loved)
            cell.selectionStyle = .none
        default:
            break
        }
        return cell!
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var heightForRows: CGFloat = CGFloat()
        switch indexPath.section {
        case 0:
            switch indexPath.row {
            case 0:
                heightForRows = 72
            default:
                break
            }
        case 1:
            switch indexPath.row {
            case 0:
                heightForRows = 147
            default:
                break
            }
        case 2:
            heightForRows = 253
        default:
            break
        }
        return heightForRows
    }
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 2:
            let titles: String = self.listProductPromo?[indexPath.row].title ?? ""
            let URLImg: String = self.listProductPromo?[indexPath.row].imageUrl ?? ""
            let loved: Int = self.listProductPromo?[indexPath.row].loved ?? 0
            let desc: String = self.listProductPromo?[indexPath.row].description ?? ""
            let price: String = self.listProductPromo?[indexPath.row].price ?? ""
            self.navigationController?.pushViewController(DetailProductPromoViewController(imgURL: URLImg, title: titles, loved: loved, description: desc, price: price, listProductPromo: listProductPromo), animated: true)
        default:
            break
        }
    }

}
