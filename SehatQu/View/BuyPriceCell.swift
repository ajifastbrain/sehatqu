//
//  BuyPriceCell.swift
//  SehatQu
//
//  Created by Macintosh on 27/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class BuyPriceCell: UITableViewCell {
    
    @IBOutlet weak var btnBuy: UIButton!
    
    @IBOutlet weak var labelPrice: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        btnBuy.layer.borderWidth = 1
        btnBuy.layer.borderColor = UIColor.black.cgColor
        btnBuy.layer.cornerRadius = 10
        btnBuy.setTitle(Localization.Label.BUY.uppercased(), for: .normal)
        btnBuy.setTitleColor(UIColor.black, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
