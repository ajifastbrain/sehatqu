//
//  SearchBarCell.swift
//  SehatQu
//
//  Created by Macintosh on 27/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class SearchBarCell: UITableViewCell {
    
    @IBOutlet weak var txtSearch: UITextField!
    
    @IBOutlet weak var btnSearch: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        txtSearch.layer.cornerRadius = 23
        txtSearch.layer.borderWidth = 1
        txtSearch.layer.borderColor = UIColor.init(hexString: "#D0CCCC").cgColor
        txtSearch.setLeftPaddingPoints(50)
        txtSearch.setRightPaddingPoints(16)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
