//
//  DescriptionViewCell.swift
//  SehatQu
//
//  Created by Macintosh on 27/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class DescriptionViewCell: UITableViewCell {

    @IBOutlet weak var labelDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        labelDesc.lineBreakMode = .byWordWrapping
        labelDesc.numberOfLines = 0
        labelDesc.font = UIFont.appFont(fontType: FontType.regular, fontSize: FontSize.L)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
