//
//  CategoryCell.swift
//  SehatQu
//
//  Created by Aji Prakosa on 27/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet weak var listCategory: UICollectionView!
    
    var dataCategory: [Category]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        listCategory.register(UINib(nibName: "CategoryItemViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoryItemViewCell")
        listCategory.delegate = self
        listCategory.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureView(dataCtg: [Category]?) {
        dataCategory = dataCtg
        listCategory.reloadData()
    }
}

extension CategoryCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataCategory?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryItemViewCell", for: indexPath as IndexPath) as! CategoryItemViewCell
        
        
        let imgUrls: String = dataCategory?[indexPath.row].imageUrl ?? ""
        let nameCategory: String = dataCategory?[indexPath.row].name ?? ""
        cell.configureView(imgURL: imgUrls, title: nameCategory)
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 112, height: 147)
    }
}
