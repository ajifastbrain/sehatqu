//
//  HeaderImageCell.swift
//  SehatQu
//
//  Created by Macintosh on 27/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import SDWebImage

class HeaderImageCell: UITableViewCell {
    
    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var btnShare: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureImage(imgURL: String) {
        imgProduct.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgProduct.sd_setImage(with: URL(string: imgURL), placeholderImage: nil, options: SDWebImageOptions.retryFailed)
    }
    
}
