//
//  ProductPromoCell.swift
//  SehatQu
//
//  Created by Aji Prakosa on 27/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import SDWebImage

class ProductPromoCell: UITableViewCell {
    
    @IBOutlet weak var imgProductPromo: UIImageView!
    
    @IBOutlet weak var btnImgLoved: UIButton!
    
    @IBOutlet weak var labelName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgProductPromo.layer.cornerRadius = 10
        labelName.font = UIFont.appFont(fontType: FontType.semiBold, fontSize: FontSize.M)
        labelName.numberOfLines = 0
        labelName.lineBreakMode = .byWordWrapping
    }
    
    func configureView(imgURL: String, name: String, loved: Int) {
        labelName.text = name
        imgProductPromo.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgProductPromo.sd_setImage(with: URL(string: imgURL), placeholderImage: nil, options: SDWebImageOptions.retryFailed)
        if loved == 0 {
            btnImgLoved.setImage(UIImage(named: "unloved"), for: .normal)
        } else {
            btnImgLoved.setImage(UIImage(named: "lovedred"), for: .normal)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
