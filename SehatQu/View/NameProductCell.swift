//
//  NameProductCell.swift
//  SehatQu
//
//  Created by Macintosh on 27/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit

class NameProductCell: UITableViewCell {
    
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var btnLoved: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        labelName.lineBreakMode = .byWordWrapping
        labelName.numberOfLines = 0
        labelName.font = UIFont.appFont(fontType: FontType.bold, fontSize: FontSize.LL)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureView(name: String, loved: Int) {
        labelName.text = name
        if loved == 0 {
            btnLoved.setImage(UIImage(named: "unloved"), for: .normal)
        } else {
            btnLoved.setImage(UIImage(named: "lovedred"), for: .normal)
        }
    }
}
