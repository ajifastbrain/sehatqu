//
//  CategoryItemViewCell.swift
//  SehatQu
//
//  Created by Aji Prakosa on 27/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import SDWebImage

class CategoryItemViewCell: UICollectionViewCell {
    @IBOutlet weak var imgCategory: UIImageView!
    
    @IBOutlet weak var labelNameCategory: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imgCategory.layer.cornerRadius = 10
        labelNameCategory.textAlignment = .center
        labelNameCategory.font = UIFont.appFont(fontType: FontType.regular, fontSize: FontSize.SS)
    }

    func configureView(imgURL: String, title: String) {
        labelNameCategory.text = title
        imgCategory.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgCategory.sd_setImage(with: URL(string: imgURL), placeholderImage: nil, options: SDWebImageOptions.retryFailed)
    }
}
