//
//  PurchaseCell.swift
//  SehatQu
//
//  Created by Aji Prakosa on 27/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import SDWebImage

class PurchaseCell: UITableViewCell {
    @IBOutlet weak var imgProduct: UIImageView!
    
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var labelPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        labelName.font = UIFont.appFont(fontType: FontType.regular, fontSize: FontSize.L)
        labelPrice.font = UIFont.appFont(fontType: FontType.regular, fontSize: FontSize.L)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureView(imgURL: String, name: String, price: String) {
        imgProduct.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imgProduct.sd_setImage(with: URL(string: imgURL), placeholderImage: nil, options: SDWebImageOptions.retryFailed)
        labelName.text = name
        labelPrice.text = price
    }
    
}
