//
//  AppDelegate.swift
//  SehatQu
//
//  Created by Macintosh on 26/06/20.
//  Copyright © 2020 Macintosh. All rights reserved.
//

import UIKit
import FBSDKShareKit
import FBSDKLoginKit
import FBSDKCoreKit
import Firebase
import FirebaseAuth
import FirebaseInstanceID
import GoogleSignIn
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        checkInternetConnection()
        instantiateViewController()
        
        FirebaseApp.configure()
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification),
                                               name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)

        GIDSignIn.sharedInstance().clientID = "1078905158485-93mgs69i2ten5a7q7b7pfrajo3c0dj7s.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        IQKeyboardManager.shared.enable = true
        
        let BarButtonItemAppearance = UIBarButtonItem.appearance()
        BarButtonItemAppearance.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.clear], for: .normal)
        
        return ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        let str_URL = url.absoluteString as NSString
        if str_URL.contains("957336748035084") {
            return ApplicationDelegate.shared.application(app, open: (url as URL?)!, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
        } else {
            let googleDidHandle = GIDSignIn.sharedInstance().handle(url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation: options[UIApplication.OpenURLOptionsKey.annotation])
                
            return googleDidHandle
                
        }
    }

    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print(error.localizedDescription)
            return
        }
        
        let authentication = user.authentication
        let credential = GoogleAuthProvider.credential(withIDToken: (authentication?.idToken)!, accessToken: (authentication?.accessToken)!)
        
        Auth.auth().signIn(with: credential, completion: { (user, error) in
            if error != nil {
                print(error?.localizedDescription ?? "")
                return
            }
            print("User logged in with Google....")
        })
    }
    
    private func signIn(signIn: GIDSignIn!, didDisconnectWithUser user: GIDGoogleUser!, withError error: NSError!) {
        if let error = error {
            print(error.localizedDescription)
            return
        }
        try! Auth.auth().signOut()
    }

    
    func instantiateViewController() {
        let defaults = UserDefaults.standard
        let isLoggedIns = defaults.string(forKey: defaultsKeys.keyLogin)
        
        if (isLoggedIns == "1") {
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.makeKeyAndVisible()
            
            let rootVC = HomeTabBarViewController()
            
            let navController = BaseNavigationController(rootViewController: rootVC)
            window?.rootViewController = navController
        } else {
            self.animate(vc: BaseNavigationController(rootViewController: LoginViewController()))
        }
    }
    
    func animate(vc: UIViewController) {
        if let window = self.window {
            
            let snapShotView = window.snapshotView(afterScreenUpdates: true)
            
            vc.view.addSubview(snapShotView!)
            snapShotView?.layer.zPosition = 1000
            window.rootViewController = vc
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseInOut, animations: { () -> Void in
                snapShotView?.alpha = 0.0
            }) { (finished) -> Void in
                snapShotView?.removeFromSuperview()
            }
            
            window.makeKeyAndVisible()
        }
    }
    @objc func tokenRefreshNotification(notification: NSNotification) {
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }
    }
    
    func checkInternetConnection() {
        var alertController = UIAlertController()
        if Connectivity.isConnectedToInternet() {
            alertController.dismiss(animated: true, completion: nil)
        } else {
            alertController = UIAlertController(title: "", message: "No Internet Connection!", preferredStyle: .actionSheet)
            let oppenSettingsAction = UIAlertAction(title: "OPEN SETTINGS", style: UIAlertAction.Style.default) {
                UIAlertAction in
                let settingUrl = "App-Prefs:root=WIFI"
                if let url = URL(string: "\(settingUrl)"), !url.absoluteString.isEmpty {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
                guard let url = URL(string: "\(settingUrl)"), !url.absoluteString.isEmpty else {
                    return
                }
                alertController.dismiss(animated: true, completion: nil)
            }
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
                UIAlertAction in
                alertController.dismiss(animated: true, completion: nil)
            }
            
            alertController.addAction(oppenSettingsAction)
            alertController.addAction(cancelAction)
            self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        }

    }

    func applicationWillTerminate(_ application: UIApplication)
    {
        let loginManager: LoginManager = LoginManager()
        loginManager.logOut()
    }
    
}

